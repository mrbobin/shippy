# frozen_string_literal: true

RSpec.describe Shippy do
  it "has a version number" do
    expect(Shippy::VERSION).not_to be nil
  end
end
