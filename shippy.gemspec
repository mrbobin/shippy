# frozen_string_literal: true

require_relative "lib/shippy/version"

Gem::Specification.new do |spec|
  spec.name = "shippy"
  spec.version = Shippy::VERSION
  spec.authors = ["Marius Bobin"]
  spec.email = ["marius@mbobin.me"]

  spec.summary = "Deployment wrapper around docker-compose and SSH Kit"
  spec.homepage = "https://mbobin.me/shippy"
  spec.license = "MIT"
  spec.required_ruby_version = ">= 3.0.0"

  spec.metadata["allowed_push_host"] = "https://rubygems.org"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://mbobin.me/shippy"

  spec.files = Dir["lib/**/*", "LICENSE.txt", "README.md"]
  spec.executables = %w[shippy]

  spec.require_paths = ["lib"]

  spec.add_runtime_dependency "activesupport", "~> 7.0", ">= 7.0.4.2"
  spec.add_runtime_dependency "ed25519", "~> 1.3"
  spec.add_runtime_dependency "bcrypt_pbkdf", "~> 1.1"
  spec.add_runtime_dependency "minitar", "~> 0.9"
  spec.add_runtime_dependency "thor", "~> 1.2", ">= 1.2.1"
  spec.add_dependency "sshkit", "~> 1.21"

  spec.add_development_dependency "rake", "~> 13.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "standard", "~> 1.3"
end
