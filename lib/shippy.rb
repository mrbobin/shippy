# frozen_string_literal: true

require "yaml"
require "active_support/all"
require "fileutils"
require "erb"
require "thor"
require "minitar"
require "zlib"

require_relative "shippy/version"
require_relative "shippy/secrets"
require_relative "shippy/config"
require_relative "shippy/commander"
require_relative "shippy/cli"
require_relative "shippy/compose"
require_relative "shippy/service"
require_relative "shippy/repo"
require_relative "shippy/compiler"

module Shippy
  class Error < StandardError; end

  class << self
    def define(&block)
      Shippy::Repo.define(&block)
    end

    def x
      SHIPPY.config
    end
  end
end
