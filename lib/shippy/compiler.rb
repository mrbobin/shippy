module Shippy
  class Compiler
    attr_reader :config, :app

    def initialize(app, config:)
      @app = app
      @config = config
    end

    def compile
      prep_build_dir
      write_compose_file
      write_configuration_files

      true
    end

    private

    def build_dir
      @build_dir ||= Pathname.pwd.join("builds", "apps", app.name).expand_path
    end

    def prep_build_dir
      FileUtils.rm_rf(build_dir)
      FileUtils.mkdir_p(build_dir)
    end

    def write_compose_file
      path = build_dir.join("docker-compose.yml")
      File.binwrite(path, app.to_yaml)
    end

    def write_configuration_files
      Dir.glob("apps/#{app.name}/**/*", File::FNM_DOTMATCH)
        .reject { |file| File.directory?(file) }
        .reject { |file| file.match?(/docker-compose\.(rb)?(yml)?/) }
        .each { |file| create_directory(file) }
        .each { |file| copy_file(file) }
    end

    def create_directory(file)
      path = build_dest(file)
      directory = File.dirname(path)
      FileUtils.mkdir_p(directory)
    end

    def copy_file(file)
      if file.ends_with?(".erb")
        copy_template_files(file)
      else
        FileUtils.cp(file, build_dest(file))
      end
    end

    def copy_template_files(file)
      new_path = build_dest(file.gsub(".erb", ""))
      mode = File.stat(file).mode
      template = ERB.new(File.read(file))
      result = template.result(app.get_binding)
      File.open(new_path, "wb", mode) { |f| f.write(result) }
    end

    def build_dest(path)
      build_dir.join("..", "..", path).expand_path
    end
  end
end
