class Shippy::Cli::Prune < Shippy::Cli::Base
  desc "all", "Prune unused images and stopped containers"
  def all
    containers
    images
  end

  desc "images", "Prune unused images older than 7 days"
  def images
    on(SHIPPY.host) do
      execute :docker, :image, :prune, "--all", "--force", "--filter", "until=#{7.days.in_hours}h"
    end
  end

  desc "containers", "Prune stopped containers older than 3 days"
  def containers
    on(SHIPPY.host) do
      execute :docker, :container, :prune, "--force", "--filter", "until=#{3.days.in_hours}h"
    end
  end
end
