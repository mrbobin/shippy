class Shippy::Cli::App < Shippy::Cli::Base
  argument :app_name, type: :string

  def initialize(...)
    super(...)
    load_app
  end

  desc "deploy", "Deploy application"
  def deploy
    say "Deploying #{app_name} on #{SHIPPY.host}...", :magenta

    SHIPPY.compile
    archive
    backup
    upload
    create_network
    refresh
    stop_old
    pre_start_hooks
    start
    cleanup
    status
  end

  desc "refresh", "Pull new images"
  def refresh
    say "Pulling images for #{app_name} on #{SHIPPY.host}...", :magenta

    within_app do
      execute :docker, "compose", "pull"
    end
  end

  desc "start", "Start application"
  def start
    say "Starting #{app_name} on #{SHIPPY.host}...", :magenta

    within_app do
      execute :docker, "compose", "up", "-d", SHIPPY.app&.deploy_flags
    end
  end

  desc "restart", "Restart application"
  option :service, aliases: "-S", type: :string, desc: "Restart only a SERVICE"
  def restart
    say "Restarting #{app_name} on #{SHIPPY.host}...", :magenta
    restart_options = options[:service] ? ["--", options[:service]] : []

    within_app do
      execute :docker, "compose", "restart", *restart_options
    end
  end

  desc "stop", "Stop application"
  def stop
    say "Stopping #{app_name} on #{SHIPPY.host}...", :magenta

    within_app do
      execute :docker, "compose", "down", "--remove-orphans"
    end
  end

  desc "logs", "Show app logs"
  def logs
    within_app do
      puts capture(:docker, "compose", "logs", verbosity: Logger::INFO)
    end
  end

  desc "status", "Show app status"
  def status
    say "Status for #{app_name} on #{SHIPPY.host}...", :magenta

    within_app do
      puts capture(:docker, "compose", "ps", "-a")
    end
  end

  private

  def load_app
    with_app do
      SHIPPY.app = Shippy::Repo.load_app(app_name)
    end
  end

  def archive
    FileUtils.cd("builds/apps") do
      Minitar.pack(app_name, Zlib::GzipWriter.new(File.open(archive_path, "wb")))
    end
  end

  def prepare_directory
    on(SHIPPY.host) do
      execute :mkdir, "-p", SHIPPY.current_app_path
    end
  end

  def backup
    backups_path = self.backups_path

    on(SHIPPY.host) do
      if test("[ -d #{SHIPPY.current_app_path} ]")
        execute :mkdir, "-p", backups_path
        execute :mv, SHIPPY.current_app_path, backups_path
      end
    end
  end

  def stop_old
    backup_path = backups_path.join(app_name)

    on(SHIPPY.host) do
      if test("[ -f #{backup_path.join("docker-compose.yml")} ]")
        within backup_path do
          execute :docker, "compose", "down", "--remove-orphans"
        end
      end
    end
  end

  def upload
    source_path = SHIPPY.current_app_path.join("..").expand_path
    archive_path = self.archive_path
    prepare_directory

    on(SHIPPY.host) do
      tmp = capture "mktemp"
      upload! archive_path, tmp

      execute :tar, "-xzpf", tmp, "-C", source_path
      execute :rm, tmp
    end
  end

  def cleanup
    remove_archive
    remove_old_releases
  end

  def remove_archive
    archive_path = self.archive_path

    run_locally do
      execute :rm, archive_path
    end
  end

  def remove_old_releases
    app_backups_paths = backups_path.join("..").expand_path

    on(SHIPPY.host) do
      if test("[ -d #{app_backups_paths} ]")
        releases = capture(:ls, "-x", app_backups_paths).split
        directories = (releases - releases.last(SHIPPY.config.keep_releases)).map do |release|
          app_backups_paths.join(release).to_s
        end

        if directories.any?
          directories.each_slice(100) do |directories_batch|
            execute :rm, "-rf", *directories_batch
          end
        end
      end
    end
  end

  def pre_start_hooks
    say "Running #{app_name} hooks on #{SHIPPY.host}...", :magenta

    within_app do
      SHIPPY.app.each_hook do |service, options, command|
        execute :docker, "compose", "run", "--rm", options, service, command
      end
    end
  end

  def create_network
    on(SHIPPY.host) do
      result = capture("docker", "network", "ls", "-q", "-f", "'name=lan_access'")

      if result.empty?
        execute :docker, "network", "create", "lan_access"
      end
    end
  end

  def archive_path
    @archive_path ||= "/tmp/v#{Time.now.to_i}.tar.gz"
  end

  def backups_path
    @backups_path ||= SHIPPY.deploy_path.join("backups", app_name, Time.now.to_i.to_s)
  end

  def with_app
    path = Pathname.new("apps").join(app_name)

    if path.exist?
      yield
    else
      error_on_missing_app
    end
  end

  def error_on_missing_app
    raise "No app by the name of '#{app_name}'"
  end

  def within_app(&block)
    on(SHIPPY.host) do
      within(SHIPPY.current_app_path) do
        instance_eval(&block)
      end
    end
  end
end
