Shippy.define do
  service :proxy do
    image { "traefik:v2.9" }
    command { "--configFile=/config/config.yml" }

    environment do
      {
        CF_API_EMAIL: secrets(:cloudflare_email),
        CF_DNS_API_TOKEN: secrets(:cloudflare_token)
      }
    end

    ports do
      ["80:80", "443:443", "8080:8080"]
    end

    volumes do
      [
        "/var/run/docker.sock:/var/run/docker.sock",
        "./traefik:/config",
        "acme:/etc/traefik/acme"
      ]
    end

    use_default_options
  end
end
