require "thor"
require "sshkit"

module Shippy::Cli
  class Base < Thor
    include SSHKit::DSL

    def self.exit_on_failure?
      true
    end

    class_option :config_file, aliases: "-c", default: "config/shippy.yml", desc: "Path to config file"
    class_option :verbose, type: :boolean, aliases: "-v", desc: "Detailed logging"
    class_option :quiet, type: :boolean, aliases: "-q", desc: "Minimal logging"

    def initialize(*)
      super
      initialize_commander(options)
    end

    private

    def initialize_commander(options)
      SHIPPY.tap do |commander|
        commander.config_file = Pathname.new(File.expand_path(options[:config_file]))

        if options[:verbose]
          ENV["VERBOSE"] = "1" # For backtraces via cli/start
          commander.verbosity = :debug
        end

        if options[:quiet]
          commander.verbosity = :error
        end
      end
    end

    def print_runtime
      started_at = Time.now
      yield
      Time.now - started_at
    ensure
      runtime = Time.now - started_at
      puts "  Finished all in #{sprintf("%.1f seconds", runtime)}"
    end
  end
end
