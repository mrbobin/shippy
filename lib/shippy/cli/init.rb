class Shippy::Cli::Init < Thor::Group
  include Thor::Actions

  def self.exit_on_failure?
    true
  end

  def self.source_root
    File.dirname(__FILE__)
  end

  def create_config_file
    copy_file "templates/config/shippy.yml", "config/shippy.yml"
  end

  def create_secrets_file
    copy_file "templates/config/secrets.yml", "config/secrets.yml"
  end

  def init_gemfile
    return if ::File.exist?("Gemfile")

    run "bundle init", abort_on_failure: false
  end

  def add_shippy_to_gemfile
    insert_into_file "Gemfile", "gem 'shippy', '~> #{Shippy::VERSION}'"

    run "bundle install", abort_on_failure: false
  end

  def generate_binstubs
    run "bundle binstubs shippy"
  end

  def create_default_apps
    directory "templates/apps", "apps"
  end

  def init_git
    run "git init"
  end

  def genegate_gitignore
    create_file ".gitignore"
    insert_into_file ".gitignore", "/.bundle/\n"
    insert_into_file ".gitignore", "/builds/\n"
    insert_into_file ".gitignore", "/config/secrets.yml\n"
  end
end
