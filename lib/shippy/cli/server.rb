class Shippy::Cli::Server < Shippy::Cli::Base
  desc "bootstrap", "Ensure curl and Docker are installed on server"
  def bootstrap
    on(SHIPPY.host) do
      dependencies_to_install = [].tap do |dependencies|
        dependencies << "curl" unless execute "which curl", raise_on_non_zero_exit: false
        dependencies << "docker.io" unless execute "which docker", raise_on_non_zero_exit: false
        dependencies << "docker-compose" unless execute "which docker-compose", raise_on_non_zero_exit: false
      end

      if dependencies_to_install.any?
        execute "apt-get update -y && apt-get install #{dependencies_to_install.join(" ")} -y"
      end
    end
  end

  desc "setup", "Generate directories structure"
  def setup
    on(SHIPPY.host) do
      execute(:mkdir, "-p", SHIPPY.config.deploy_to + "/apps")
      execute(:mkdir, "-p", SHIPPY.config.deploy_to + "/backups")
    end
  end
end
