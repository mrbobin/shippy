require "thor"

class Shippy::Cli::Main < Shippy::Cli::Base
  desc "init", "Create config files and app setup"
  def init
    Shippy::Cli::Init.new.invoke_all
  end

  desc "version", "Show SHIPPY version"
  def version
    puts Shippy::VERSION
  end

  desc "setup", "Setup all accessories and deploy apps to server"
  def setup
    invoke "shippy:cli:server:bootstrap"
    invoke "shippy:cli:server:setup"
    deploy
  end

  desc "deploy", "Deploy all apps to server"
  def deploy
    print_runtime do
      Dir.glob("*", base: "apps").each do |name|
        Shippy::Cli::App.send :dispatch, :deploy, [name], {}, {}
      end
    end
  end

  desc "stop", "Stop all apps on server"
  def stop
    print_runtime do
      Dir.glob("*", base: "apps").each do |name|
        Shippy::Cli::App.send :dispatch, :stop, [name], {}, {}
      end
    end
  end

  desc "refresh", "Pull newer images and restart services"
  def refresh
    print_runtime do
      Dir.glob("*", base: "apps").each do |name|
        Shippy::Cli::App.send :dispatch, :refresh, [name], {}, {}
        Shippy::Cli::App.send :dispatch, :start, [name], {}, {}
        Shippy::Cli::App.send :dispatch, :status, [name], {}, {}
      end
    end
  end

  desc "app", "Manage application"
  subcommand "app", Shippy::Cli::App

  desc "server", "Bootstrap servers with curl and Docker"
  subcommand "server", Shippy::Cli::Server

  desc "prune", "Prune old application images and containers"
  subcommand "prune", Shippy::Cli::Prune
end
