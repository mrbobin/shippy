module Shippy
  class Secrets
    attr_reader :data

    def initialize(file)
      @data = YAML.load_file(file).deep_symbolize_keys
    end

    def fetch(app, name)
      data
        .fetch(app.to_sym)
        .fetch(name.to_sym)
    end
  end
end
