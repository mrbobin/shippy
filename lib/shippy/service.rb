module Shippy
  class Service
    ATTRIBUTES = %i[
      build command depends_on devices entrypoint environment hostname image
      labels logging networks user ports restart volumes volumes_from working_dir
      security_opt
    ].freeze

    attr_reader :name, :app

    ATTRIBUTES.each do |name|
      define_method(name) do |&block|
        if block
          @data.merge!({name => block.call}.compact)
        else
          @data[name]
        end
      end
    end

    def initialize(name, app:, &block)
      @name = name
      @app = app
      @data = {}

      instance_eval(&block)
    end

    def to_hash
      {@name => @data}
    end

    def use_default_options
      use_default_restart
      use_default_networks
      use_default_logging
    end

    def use_default_restart
      restart { "unless-stopped" }
    end

    def use_default_logging
      logging do
        {
          driver: "json-file",
          options: {
            "max-size": "10m",
            "max-file": "5"
          }
        }
      end
    end

    def use_default_networks
      networks { ["lan_access"] }
    end

    def use_traefik(name:, alt: nil, port: nil, healthcheck: false)
      labels do
        all_labels = ["traefik.enable=true"]
        all_labels += ["traefik.docker.network=lan_access"]
        all_labels += build_traefik_labels(name: name, port: port)
        all_labels += build_traefik_labels(name: alt, port: port) if alt
        all_labels += build_ihxator_labels(service: "https://#{name}.#{wildcard_domain}") if healthcheck

        all_labels
      end
    end

    def build_traefik_labels(name:, port: nil)
      [
        "traefik.http.routers.websecure-#{name}.rule=Host(`#{name}.#{wildcard_domain}`)",
        "traefik.http.routers.websecure-#{name}.entrypoints=websecure",
        "traefik.http.routers.websecure-#{name}.tls=true",
        "traefik.http.routers.websecure-#{name}.tls.certresolver=ssl-resolver",
        "traefik.http.routers.websecure-#{name}.tls.domains[0].main=#{wildcard_domain}",
        "traefik.http.routers.websecure-#{name}.tls.domains[0].sans=*.#{wildcard_domain}",

        "traefik.http.routers.web-#{name}.rule=Host(`#{name}.#{wildcard_domain}`)",
        "traefik.http.routers.web-#{name}.entrypoints=web",
        "traefik.http.routers.web-#{name}.middlewares=secure-redirect-scheme@file",

        "traefik.http.routers.#{name}-local.rule=Host(`#{name}.#{local_domain}`)",
        "traefik.http.routers.#{name}-local.entrypoints=web",
        "traefik.http.middlewares.#{name}-local.redirectregex.regex=^http://#{name}.#{local_domain}",
        "traefik.http.middlewares.#{name}-local.redirectregex.replacement=https://#{name}.#{wildcard_domain}",
        "traefik.http.middlewares.#{name}-local.redirectregex.permanent=true",
        "traefik.http.routers.#{name}-local.middlewares=#{name}-local@docker"
      ].tap do |labels|
        if port
          labels << "traefik.http.services.#{name}.loadbalancer.server.port=#{port}"
          labels << "traefik.http.routers.websecure-#{name}.service=#{name}"
          labels << "traefik.http.routers.web-#{name}.service=#{name}"
          labels << "traefik.http.routers.#{name}-local.service=#{name}"
        end
      end
    end

    def build_ihxator_labels(service:)
      [
        "ihxator.enable=true",
        "ihxator.service=#{service}"
      ]
    end

    def secrets(name)
      @app.secrets(name)
    end

    delegate :x, to: Shippy
    delegate :wildcard_domain, :local_domain, to: :x

    def hooks
      if block_given?
        @hooks = normalize_hooks(yield)
      else
        @hooks.to_a.map { |data| [name, data[:options], data[:cmd]] }
      end
    end

    private

    def normalize_hooks(data)
      Array
        .wrap(data)
        .map { |data| data.is_a?(String) ? {cmd: data} : data }
    end
  end
end
