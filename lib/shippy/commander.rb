module Shippy
  class Commander
    attr_accessor :config_file, :verbosity, :repo, :app
    delegate :host, :deploy_path, to: :config

    def initialize(config_file: nil, verbosity: :info)
      @config_file, @verbosity = config_file, verbosity
    end

    def config
      @config ||= Shippy::Config
        .new(config_file)
        .tap { |config| configure_sshkit_with(config) }
    end

    def with_verbosity(level)
      old_level = verbosity

      self.verbosity = level
      SSHKit.config.output_verbosity = level

      yield
    ensure
      self.verbosity = old_level
      SSHKit.config.output_verbosity = old_level
    end

    def compile
      Shippy::Compiler.new(app, config: config).compile
    end

    def current_app_path
      deploy_path.join("apps", app.name)
    end

    private

    def configure_sshkit_with(config)
      SSHKit::Backend::Netssh.configure { |ssh| ssh.ssh_options = config.ssh_options }
      SSHKit.config.command_map[:docker] = "docker"
      SSHKit.config.command_map[:docker_compose] = "docker-compose"
      SSHKit.config.output_verbosity = verbosity
    end
  end
end
