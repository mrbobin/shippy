module Shippy
  class Config
    attr_reader :data

    def initialize(file)
      @data = YAML.load_file(file).deep_symbolize_keys
      @secrets = Secrets.new(@data.fetch(:secrets_file))
    end

    def secrets(app, name)
      @secrets.fetch(app, name)
    end

    def ssh_options
      {user: ssh_user, auth_methods: ["publickey"]}.compact
    end

    def ssh_user
      if data[:ssh].present?
        data.dig(:ssh, :user) || "root"
      else
        "root"
      end
    end

    def deploy_path
      Pathname.new(deploy_to)
    end

    def method_missing(name, *args)
      if data.key?(name)
        data.fetch(name)
      else
        super
      end
    end

    def respond_to_missing?(name, include_private = false)
      data.key?(name) || super
    end
  end
end
