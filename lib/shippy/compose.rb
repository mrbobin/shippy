module Shippy
  class Compose
    attr_reader :services, :name

    def initialize(name, &block)
      @name = name
      @services = []
      instance_eval(&block)
    end

    def service(name, &block)
      service = Service.new(name, app: self, &block)
      @services << service
    end

    def xservice(name, &block)
      puts "Service #{name} is disabled"
    end

    def deploy_flags
      if block_given?
        @deploy_flags = yield
      else
        @deploy_flags
      end
    end

    def to_hash
      data = {}
      data[:services] = services.map(&:to_hash).reduce(:merge)
      data[:volumes] = volumes_hash.presence
      data[:networks] = networks_hash.presence
      data.compact
    end

    def to_yaml
      to_hash.deep_stringify_keys.to_yaml
    end

    def each_hook(&block)
      @services
        .flat_map(&:hooks)
        .each(&block)
    end

    def secrets(secret_name)
      Shippy.x.secrets(name, secret_name)
    end

    def get_binding
      binding
    end

    delegate :x, to: Shippy
    delegate :wildcard_domain, :local_domain, to: :x

    private

    def networks
      services.flat_map(&:networks).uniq.compact
    end

    def networks_hash
      data = networks.reduce({}) { |acc, net| acc.merge(net => nil) }
      data.deep_symbolize_keys!
      data[:lan_access] = {external: true} if data.key?(:lan_access)
      data
    end

    def volumes
      services
        .flat_map(&:volumes)
        .uniq
        .compact
        .select { |v| v.start_with?(/[a-z]/) }
        .map { |v| v.split(":", 2).first }
    end

    def volumes_hash
      volumes.reduce({}) { |acc, v| acc.merge(v => nil) }
    end
  end
end
