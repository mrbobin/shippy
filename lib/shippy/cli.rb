require_relative "cli/base"
require_relative "cli/init"
require_relative "cli/server"
require_relative "cli/app"
require_relative "cli/prune"
require_relative "cli/main"

module Shippy::Cli
end

SHIPPY = Shippy::Commander.new
