module Shippy
  class Repo
    attr_reader :data

    class << self
      def current
        Thread.current[:repo]
      end

      def current=(repo)
        Thread.current[:repo] = repo
      end

      def with(repo)
        old_repo = current

        self.current = repo
        yield
      ensure
        self.current = old_repo
      end

      def define(&block)
        current.define(&block)
      end

      def load_app(name)
        repo = new
        repo.load_definitions(pattern: name)
        repo.data.fetch(name)
      end
    end

    def initialize
      @data = {}
    end

    def define(&block)
      name = caller
        .reject { |path| path.match(/lib\/shippy/) }
        .find { |path| path.match(/docker-compose\.rb/) }.split(":", 2).first
      name = File.basename(File.dirname(name))
      data[name] = Compose.new(name, &block)
    end

    def load_definitions(pattern: "**")
      self.class.with(self) do
        Pathname.glob("apps/#{pattern}/docker-compose.rb")
          .map(&:expand_path)
          .each { |path| load path }
      end
    end

    def hooks_for(app)
      data.fetch(app).hooks
    end

    def each(&block)
      data.each(&block)
    end

    def each_app(&block)
      data.values.each(&block)
    end

    def app_names
      data.keys
    end
  end
end
