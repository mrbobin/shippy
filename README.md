# Shippy

Container orchestration solution for homelabs inspired by [Capistrano](https://capistranorb.com/) and [mrsk](https://github.com/mrsked/mrsk) and built on top of [docker-compose](https://docs.docker.com/compose/).

## Features

- Ruby DSL to generate the docker-compose YAML files
- Centralized source to define secrets
- Uses Traefik as a reverse proxy with SSL wildcard support

## Installation

Install the gem and add to the application's Gemfile by executing:

```shell
$ bundle add shippy
```

Or

```shell
$ gem install shippy
```

## Usage

In you homelab directory execute:

```shell
homelab$ bundle exec shippy init
```

Or

```shell
homelab$ shippy init
```

This will generate the config files and the `apps` directory with Traefik in the `proxy` app. This can be used as a guide for other apps.

Now `shippy` can be called using `bin/shippy` binstub and the other commands can be listed using `bin/shippy -h`.

### Docker compose DSL:

Conventions:

- the file needs to be located at `apps/app_name/docker-compose.rb`
- the `Shippy.define {}` block describes multiple services for the same compose file
- `service :name` describes a docker service
- most of the docker-compose keywords are implemented as [methods](./lib/shippy/service.rb) that accept a block.
- `volumes` and `networks` are automatically defined

Example:

```ruby
Shippy.define do
  service :proxy do
    image { "traefik:v2.9" }
    command { "--configFile=/config/config.yml" }

    environment do
      {
        CF_API_EMAIL: secrets(:cloudflare_email),
        CF_DNS_API_TOKEN: secrets(:cloudflare_token)
      }
    end

    ports do
      ["80:80", "443:443", "8080:8080"]
    end

    volumes do
      [
        "/var/run/docker.sock:/var/run/docker.sock",
        "./traefik:/config",
        "acme:/etc/traefik/acme"
      ]
    end

    use_default_options
  end
end
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and merge requests are welcome on GitLab at https://gitlab.com/mrbobin/shippy. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](./CODE_OF_CONDUCT.md).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Shippy project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](./CODE_OF_CONDUCT.md).
